const express = require('express');

const app = express();

const mongoose = require('mongoose');

const config = require('./config');
const cors = require('cors');

mongoose.connect("mongodb+srv://admin:123123123@cluster0-jixmk.mongodb.net/amapp?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: true
}).then(() => {
	console.log("Remote Database connection Established");
});

app.use(express.urlencoded({extended: false}));

app.use(express.json());
app.use(cors());

app.listen(config.port, () => {
	console.log(`Listening on port ${config.port}`);
});


const assets = require('./routes/asset_router');

app.use('/admin', assets);
