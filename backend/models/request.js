const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const moment = require('moment');

const RequestSchema = new Schema({
	code: String,
	status: {
		type: string,
		default: 'Pending'
	},
	quantity: String,
	request_date: {
		type: String,
		default: Date.now()
	},
	logs: [
		{
			status: {
				type: String,
				default: "Pending"
			},
			update_date: {
				type: Date,
				default: moment(Date.now()).format('MM/DD/YYYY');
			},
			message: String
		}
	],
	asset_name: String,
	asset_id: String,
	requestor: String,
	requestor_id: String,
	approver: String,
	approver_id: String
});

module.exports = mongoose.model('Request', RequestSchema);