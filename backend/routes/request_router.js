const express = require('express');

const RequestRouter = express.Router();

const RequestModel = require('../models/request');

RequestRouter.post('/add-request', async (request, response) => {
	try {
		let request = RequestModel({
			code: request.body.code,
			status: request.body.status,
			quantity: request.body.quantity,
			request_date: request.body.request_date,
			logs: request.body.logs,
			asset_name: request.body.asset_name,
			asset_id: request.body.asset_id,
			requestor: request.body.requestor,
			requestor_id: request.body.requestor_id,
			approver: request.body.approver,
			approver_id: request.body.approver_id
		});
		request = await request.save();
		response.send(request);
	} catch(e) {
		console.log(e);
	}
});

// RequestRouter.get('/show-assets', async (request, response) => {
// 	try {
// 		let assets = await RequestModel.find({});
// 		response.send(assets);
// 	} catch(e) {
// 		console.log(e);
// 	}
// });

// RequestRouter.get('/show-asset/:id', async (request, response) => {
// 	try {
// 		let asset = await RequestModel.findById(request.params.id);
// 		response.send(asset);
// 	} catch(e) {
// 		console.log(e);
// 	}
// });

// RequestRouter.put('/update-asset/:id', async (request, response) => {
// 	try {
// 		let asset = await RequestModel.findById(request.params.id);
// 		if (!asset) {
// 			return response
// 					.status(404)
// 					.send(`Asset can't be found`);
// 		}
// 		let condition = {_id: request.params.id};
// 		let updates = {
// 			name: request.body.name,
// 			description: request.body.description,
// 			category: request.body.category,
// 			stock: request.body.stock
// 		}

// 		let updatedAsset = await RequestModel.findOneAndUpdate(condition, updates, {new: true});
// 		response.send(updatedAsset);
// 	} catch(e) {
// 		console.log(e);
// 	}
// });

// RequestRouter.delete('/delete-asset/:id', async (request, response) => {
// 	try {
// 		let asset = await RequestModel.findByIdAndDelete(request.params.id);
// 		response.send(asset);
// 	} catch(e) {
// 		console.log(e);
// 	}
// });

module.exports = RequestRouter;