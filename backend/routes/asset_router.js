const express = require('express');

const AssetRouter = express.Router();

const AssetModel = require('../models/asset');

AssetRouter.post('/add-asset', async (request, response) => {
	try {
		let asset = AssetModel({
			name: request.body.name,
			description: request.body.description,
			serial_number: request.body.serial_number,
			category: request.body.category,
			stock: request.body.stock
		});
		asset = await asset.save();
		response.send(asset);
	} catch(e) {
		console.log(e);
	}
});

AssetRouter.get('/show-assets', async (request, response) => {
	try {
		let assets = await AssetModel.find({});
		response.send(assets);
	} catch(e) {
		console.log(e);
	}
});

AssetRouter.get('/show-asset/:id', async (request, response) => {
	try {
		let asset = await AssetModel.findById(request.params.id);
		response.send(asset);
	} catch(e) {
		console.log(e);
	}
});

AssetRouter.put('/update-asset/:id', async (request, response) => {
	try {
		let asset = await AssetModel.findById(request.params.id);
		if (!asset) {
			return response
					.status(404)
					.send(`Asset can't be found`);
		}
		let condition = {_id: request.params.id};
		let updates = {
			name: request.body.name,
			description: request.body.description,
			category: request.body.category,
			stock: request.body.stock
		}

		let updatedAsset = await AssetModel.findOneAndUpdate(condition, updates, {new: true});
		response.send(updatedAsset);
	} catch(e) {
		console.log(e);
	}
});

AssetRouter.delete('/delete-asset/:id', async (request, response) => {
	try {
		let asset = await AssetModel.findByIdAndDelete(request.params.id);
		response.send(asset);
	} catch(e) {
		console.log(e);
	}
});

AssetRouter.patch('/update-stock/:id', async (request, response) => {
	try {
		let condition = {_id: request.params.id};
		let update = {
			stock: request.body.stock
		}

		let updated = await AssetModel.findOneAndUpdate(condition, update, {new: true});
		response.send(updated);
	} catch(e) {
		console.log(e);
	}
});

module.exports = AssetRouter;