import React, {useEffect, useState} from 'react';
import {AssetRow} from './components';
import {Loading} from '../../components';
import {Button} from 'reactstrap';
import {AssetForm} from './components'
import moment from 'moment';
import axios from 'axios';
import {ToastContainer, toast} from 'react-toastify';

const invalidStock = () => {
	toast.error('Invalid Stock');
}

const Assets = () => {

	const [assets, setAssets] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [showForm, setShowForm] = useState(false);
	const [nameRequired, setNameRequired] = useState(true);
	const [name, setName] = useState('');
	const [descriptionRequired, setDescriptionRequired] = useState(true);
	const [description, setDescription] = useState('');
	const [categoryRequired, setCategoryRequired] = useState(true);
	const [category, setCategory] = useState('');
	const [stockRequired, setStockRequired] = useState(true);
	const [stock, setStock] = useState(0);
	const [showStock, setShowStock] = useState(true);
	const [editID, setEditID] = useState('');

	useEffect(() => {

		fetch('http://localhost:4000/admin/show-assets')
			.then(response => response.json())
			.then(data => {
				setAssets(data);
				setIsLoading(false);
			});
	}, []);

	const handleShowForm = () => {
		setShowForm(!showForm);
	}

	const handleAssetNameChange = (e) => {
		if (e.target.value === "") {
			setNameRequired(true);
		} else {
			setNameRequired(false);
			setName(e.target.value);
		}
	}

	const handleAssetDescriptionChange = (e) => {
		if (e.target.value === "") {
			setDescriptionRequired(true);
		} else {
			setDescriptionRequired(false);
			setDescription(e.target.value);
		}
	}

	const handleAssetCategoryChange = (e) => {
		if (e.target.value === "") {
			setCategoryRequired(true);
		} else {
			setCategoryRequired(false);
			setCategory(e.target.value);
		}
	}

	const handleAssetStockChange = (e) => {
		if (e.target.value === "") {
			setStockRequired(true);
		} else {
			setStockRequired(false);
			setStock(e.target.value);
		}
	}

	const handleSaveAsset = () => {

		let serialNumber = moment(new Date()).format('x');

		axios({
			method: 'POST',
			url: 'http://localhost:4000/admin/add-asset',
			data: {
				name,
				description,
				category,
				stock: parseInt(stock),
				serial_number: serialNumber
			}
		}).then(response=>{
			let newAssets = [...assets];
			newAssets.push(response.data);
			setAssets(newAssets);
		});

		refresh();
	}

	const refresh = () => {
		setShowForm(false);
		setName("");
		setDescription("");
		setCategory("");
		setStock(0);
		setNameRequired(true);
		setDescriptionRequired(true);
		setCategoryRequired(true);
		setStockRequired(true);
	}

	const deleteAsset = (id) => {
		axios({
			method: 'DELETE',
			url: 'http://localhost:4000/admin/delete-asset/'+id
		})
		.then(response => {
			let newAssets = assets.filter(asset => asset._id !== response.data._id);
			setAssets(newAssets);
		});
	}

	const editStock = (e, id) => {
		let stock = e.target.value;
		if (stock <= 0) {
			setShowStock(true);
			invalidStock();
		} else {
			axios({
				method: 'PATCH',
				url: 'http://localhost:4000/admin/update-stock/'+id,
				data: {
					stock
				}
			})
			.then(response => {
				
				let oldIndex;
				assets.forEach((asset, index) => {
					if (asset._id === id) {
						oldIndex = index;
					}
				});

				let newAssets = [...assets];
				newAssets.splice(oldIndex, 1, response.data);
				setAssets(newAssets);
				setShowStock(true);
				setEditID('');
			});
		}
	}

	const stockEditInput = (id) => {
		setEditID(id);
	}


	return (
		<React.Fragment>
		<ToastContainer />
		{isLoading ? 
			<Loading />
		:
			<React.Fragment>
				<div>
					<h1 className="text-center py-5">Assets</h1>
					<div className="d-flex justify-content-center">
						<Button 
							onClick={handleShowForm}
							color="success">Add Asset</Button>
						<AssetForm
							showForm={showForm}
							handleShowForm={handleShowForm}
							handleAssetNameChange={handleAssetNameChange}
							nameRequired={nameRequired}
							handleAssetDescriptionChange={handleAssetDescriptionChange}
							descriptionRequired={descriptionRequired}
							handleAssetCategoryChange={handleAssetCategoryChange}
							categoryRequired={categoryRequired}
							handleAssetStockChange={handleAssetStockChange}
							stockRequired={stockRequired}
							name={name}
							description={description}
							category={category}
							stock={stock}
							handleSaveAsset={handleSaveAsset}
						 />
					</div>
				</div>
				<table className="table table-striped col-lg-10 offset-lg-1">
					<thead>
						<tr>
							<th>Serial No.</th>
							<th>Name</th>
							<th>Description</th>
							<th>Category</th>
							<th>Stock</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{assets.map(asset => {
							return <AssetRow 
										key={asset._id} 
										asset={asset}
										deleteAsset={deleteAsset}
										showStock={showStock}
										setShowStock={setShowStock}
										editStock={editStock}
										stockEditInput={stockEditInput}
										editID={editID}
										/>
						})}
					</tbody>
				</table>
			</React.Fragment>
		}
		</React.Fragment>
	);
}

export default Assets;
