
import React from 'react';
import {Modal, ModalHeader, ModalBody, Button} from 'reactstrap';
import {FormInput} from '../../../components';


const AssetForm = (props) => {
	return (
		<Modal
			isOpen={props.showForm}
			toggle={props.handleShowForm}
		>
			<ModalHeader
				toggle={props.handleShowForm}
				style={{'backgroundColor': 'salmon', 'color': 'white'}}
			>
				Add Asset
			</ModalHeader>
			<ModalBody>
				<FormInput 
					label={'Asset Name'}
					type={'text'}
					name={'name'}
					placeholder={'Enter Name'}
					onChange={props.handleAssetNameChange}
					required={props.nameRequired}
				/>
				<FormInput 
					label={'Asset Description'}
					type={'text'}
					name={'description'}
					placeholder={'Enter Description'}
					onChange={props.handleAssetDescriptionChange}
					required={props.descriptionRequired}
				/>
				<FormInput 
					label={'Asset Category'}
					type={'text'}
					name={'category'}
					placeholder={'Enter Category'}
					onChange={props.handleAssetCategoryChange}
					required={props.categoryRequired}
				/>
				<FormInput 
					label={'Asset Stock'}
					type={'number'}
					name={'stock'}
					placeholder={'Enter Stock'}
					onChange={props.handleAssetStockChange}
					required={props.stockRequired}
				/>
				<Button
					color="warning"
					onClick={props.handleSaveAsset}
					disabled={props.name === "" || props.description === "" || props.category === "" || props.stock ==="" || props.stock <= 0 ? true: false}
					>
					Save Asset
				</Button>
			</ModalBody>
		</Modal>
	);
}

export default AssetForm;