import React from 'react';
import {Button} from 'reactstrap';

import {FormInput} from '../../../components';



const AssetRow = props => {

	const asset = props.asset;

	return (
		<React.Fragment>
			
			<tr>
				<td>{asset.serial_number}</td>
				<td>{asset.name}</td>
				<td>{asset.description}</td>
				<td>{asset.category}</td>
				<td
					onClick={() => props.stockEditInput(asset._id)}
				>{props.showStock && props.editID !== asset._id ? asset.stock: <FormInput 
												type={'number'}
												defaultValue={asset.stock}
												onBlur={(e) => props.editStock(e, asset._id)}/>}</td>
				<td>
					<Button 
						color="danger"
						onClick={() => props.deleteAsset(asset._id)}>
						Delete Asset
					</Button>
				</td>
			</tr>
		</React.Fragment>
	);
}

export default AssetRow;