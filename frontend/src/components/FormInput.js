
import React from 'react';

import {FormGroup, Label, Input} from 'reactstrap';

const FormInput = ({onBlur, label, type, name, placeholder, defaultValue, onChange, required, ...props}) => {
	return (
		<React.Fragment>
			<FormGroup>
				<Label>{label}</Label>
				<Input
					type={type}
					name={name} 
					placeholder={placeholder}
					onChange={onChange}
					onBlur = {onBlur}
					defaultValue={defaultValue}
					style={required ? {border: 'solid 1px red'}: null}/>
			</FormGroup>
		</React.Fragment>
	);
}

export default FormInput;